##Angular and Symfony Preconfigured Authentification

### Dev environnement needed :

```bash
    PHP 8.0^
    composer 2^
    node 14^
    angular cli
    symfony cli
```

### Installation process :
####Symfony

```
- composer install
```

```
Configure your Database in a .env.local file, exemple :
DATABASE_URL="mysql://root@127.0.0.1:3306/my_symfony?serverVersion=5.7"

Run the following command:
symfony console d:d:c
symfony console d:m:m
symfony serve (copy the url of the server)
```

####Angular

```
- npm install
```

```
Go to the src/environments/environment.ts :
paste the url of the symfony server into urlBackClient, exemple : 

export const environment = {
    production: false,
    urlBackClient: 'https://127.0.0.1:8000'
};

Run the following command : 
ng serve
```

At this point you are good to go ! You can go to the angular app and create a user !

###Useful stuff Angular :
```
Anywhere in you application you can call getUser() form the AuthService, it will give you null or an user based on this interface :

export interface UserInterface {
    token: string;
    roles: Array<string>;
    username: string;
}
```

###Useful stuff Symfony :
```
If you want to setup generic ROLE_ADMIN for an user you can just run this command :
symfony console app:set-admin toto@gmail.com.

All route starting with ^/api require a token (already setup in the angular app if you are log in).
So in order to manage the authenticate stuff of your app in the right way, just start all your route with a /api.
```