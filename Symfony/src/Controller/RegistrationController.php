<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    #[Route('/api/register', name: 'app_register')]
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $userInfoDecoded = json_decode($request->getContent());
        $user = new User();
        $passwordEncoded = $passwordEncoder->encodePassword(
            $user,
            $userInfoDecoded->password
        );

        $user->setEmail($userInfoDecoded->email);
        $user->setPassword($passwordEncoded);

        try {
            $this->em->persist($user);
            $this->em->flush();
            return $this->json([]);
        } catch (\Exception $e) {
            //---Error handler --- return status code 401 if email already exist, else return generic code 402
            if ($e->getCode() === 1062) {
                return $this->json([], 401);
            } else {
                return $this->json([], 402);
            }
        }
    }
}
