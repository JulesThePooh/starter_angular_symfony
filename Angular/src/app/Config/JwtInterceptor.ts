import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserInterface} from "../Interface/userInterface";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // add authorization header with jwt token if available
        let userJson: string | null = localStorage.getItem('user');
        if (userJson !== null) {
            let user: UserInterface | null = JSON.parse(userJson);
            let jwt = user?.token;
            if (jwt) {
                request = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${jwt}`
                    }
                });
            }
        }


        return next.handle(request);
    }
}
