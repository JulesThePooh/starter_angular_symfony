import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {JwtInterface} from "../Interface/jwtInterface";
import jwtDecode from "jwt-decode";
import {UserInterface} from "../Interface/userInterface";
import {JwtDecodedInterface} from "../Interface/jwtDecodedInterface";
import {Router} from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private _httpClient: HttpClient, private _router: Router) {
    }

    login(email: string, password: string): void | boolean {

        this._httpClient.post<JwtInterface>(`${environment.urlBackClient}/api/login_check`, {email, password})
            .subscribe((data) => {
                    //check if someone was connected and remove it
                    if (localStorage.getItem('user') !== null) {
                        localStorage.removeItem('user');
                    }

                    //decode token to get user info
                    let decoded: JwtDecodedInterface = jwtDecode(data.token);

                    //create the standard user object
                    let user: UserInterface = {
                        token: data.token,
                        roles: decoded.roles,
                        username: decoded.username
                    };
                    localStorage.setItem('user', JSON.stringify(user));
                    this._router.navigate(['/']).then(() => {
                        window.location.reload();
                    })
                },
                (error) => {
                    this._router.navigateByUrl('/login?error=invalid').then(() => {
                        window.location.reload();
                    })
                }
            )
    }

    logout() {
        if (localStorage.getItem('user') !== null) {
            localStorage.removeItem('user');
        }
        this._router.navigate(['/']).then(() => {
            window.location.reload();
        })
    }

    getUser(): UserInterface | null {
        let userJson: string | null = localStorage.getItem('user');
        if (userJson !== null) {
            return JSON.parse(userJson);
        } else {
            return null;
        }
    }
}
