export interface JwtDecodedInterface {
    exp: number;
    iat: number;
    roles: Array<string>;
    username: string;
}
