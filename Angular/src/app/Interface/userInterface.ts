export interface UserInterface {
    token: string;
    roles: Array<string>;
    username: string;
}
