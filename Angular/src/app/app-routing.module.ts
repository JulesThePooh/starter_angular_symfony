import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./Component/page/home/home.component";
import {LogoutComponent} from "./Component/util/logout/logout.component";
import {LoginComponent} from "./Component/page/login/login.component";
import {RegisterComponent} from "./Component/page/register/register.component";

const routes: Routes = [
    {path: 'register', component: RegisterComponent},
    {path: 'login', component: LoginComponent},
    {path: 'logout', component: LogoutComponent},
    {path: '', component: HomeComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
