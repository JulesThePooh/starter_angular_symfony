import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    public _errorAlreadyExist: boolean = false;
    public _errorUnknow: boolean = false;

    public _registerForm = new FormGroup({
            _email: new FormControl('', [
                Validators.required,
                Validators.email
            ]),
            _password: new FormControl('', [
                Validators.required,
                Validators.minLength(5)
            ]),
            _confirmPassword: new FormControl('', [
                Validators.required,
            ]),
        },
        {
            validators: (control) => {
                if (control.value._password !== control.value._confirmPassword) {
                    control.get("_confirmPassword")?.setErrors({notSame: true});
                }
                return null;
            },
        }
    );

    constructor(private _httpClient: HttpClient, private _router: Router) {
    }

    ngOnInit(): void {
    }

    onRegisterFormSubmit() {
        this.registerRequest(this._registerForm.value._email, this._registerForm.value._password)
    }

    registerRequest(email: string, password: string) {
        this._httpClient.post(`${environment.urlBackClient}/api/register`, {email, password})
            .subscribe((data) => {
                    this._router.navigateByUrl('/login?registration=success').then(() => {
                        window.location.reload();
                    })
                },
                (error) => {
                    if (error.status === 401) {
                        this._errorAlreadyExist = true
                    } else {
                        this._errorUnknow = true
                    }
                })
    }

    get confirmPassword() {
        return this._registerForm.get('_confirmPassword');
    }

    get password() {
        return this._registerForm.get('_password');
    }

    get email() {
        return this._registerForm.get('_email');
    }


}
