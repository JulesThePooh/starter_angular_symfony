import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../Service/auth.service";
import {FormGroup, FormControl} from '@angular/forms';
import {Validators} from '@angular/forms';
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


    public _loginForm = new FormGroup({
        _email: new FormControl('', [
            Validators.required,
            Validators.email
        ]),
        _password: new FormControl('', [
            Validators.required,
            Validators.minLength(5)
        ]),
    });

    public _error: string | null = null
    public _registerSuccess: string | null = null;

    constructor(private _authService: AuthService, private _activatedRoute: ActivatedRoute) {

    }

    ngOnInit(): void {
        this._activatedRoute.queryParams
            .subscribe(params => {
                    if (params['error'] !== undefined) {
                        this._error = params['error'];
                    }
                    if (params['registration'] !== undefined) {
                        this._registerSuccess = params['registration'];
                    }
                }
            );

    }

    onLoginFormSubmit() {
        this.login(this._loginForm.value._email, this._loginForm.value._password);
    }

    login(email: string, password: string) {
        this._authService.login(email, password);
    }

    get password() {
        return this._loginForm.get('_password');
    }

    get email() {
        return this._loginForm.get('_email');
    }

}
